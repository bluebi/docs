# Allgemeine Geschäftsbedingungen - Blue Investments

## Präambel
Die folgenden Allgemeinen Geschäftsbedingungen (nachfolgend AGB) gelten für die Inanspruchnahme der Dienste der Blue Investments GmbH (kurz: Betreiber) wie über die Webseite: www.blue.bi und sind ebenfalls Bestandteil jedes zwischen uns als Auftraggeber (kurz: AG) und dem Auftragnehmer (kurz: AN) abgeschlossenen Vertrages. 

Die vom Betreiber geschuldeten Leistungen können auch von einem mit ihm verbundenen Unternehmen, insbesondere Tochterunternehmen erbracht werden.

Etwaige Abweichungen von diesen AGB haben nur Wirksamkeit, wenn sie von uns schriftlich bestätigt werden.

Sollten einzelne Bestimmungen dieser AGB unwirksam sein, so berührt dies die übrigen Bestimmungen und der unter ihrer Zugrundelegung abgeschlossenen Verträge nicht. Die unwirksame Bestimmung ist durch jene Bestimmung, die deren Sinn und Zweck am nächsten kommt, zu ersetzen.

Alle Daten auf www.blue.bi sind unverbindlich und stellen keine Finanzanalyse, Anlageempfehlung oder sonstige rechtsverbindliche Auskunfts- oder Beratungsleistung durch den Betreiber dar. Einen Irrtum in Bezug auf Zahlenangaben behält sich der Betreiber ausdrücklich vor. Eine Haftung für eine auf Basis der Informationen des Betreibers getroffene Investmententscheidung ist ausgeschlossen.

Alle Unternehmensinformationen sind vergangenheitsbezogen und diese lassen keine verlässliche Prognose für die tatsächliche Unternehmensentwicklung zu. Prognosen sind nur Schätzungen zum Zeitpunkt der Erstellung, die tatsächliche Unternehmensentwicklung kann nicht garantiert werden. Die Ertragschancen stehen einem hohen Risiko des vollständigen Kapitalverlusts gegenüber. 

Der Erfolg eines Investments kann auch von dessen Dauer sowie von Gebühren, Spesen und Steuern abhängen. Jede Investmententscheidung bedarf einer individuellen Anpassung an die persönlichen und/oder steuerrechtlichen Verhältnisse des Investors. 

Die Angaben auf der Website ersetzen kein Beratungsgespräch mit einem Anlage- oder Steuerberater. Für die Vollständigkeit und Richtigkeit der Angaben wird keine Gewähr übernommen.

Ein Anspruch auf Registrierung und Nutzung der Website besteht nicht. Es steht dem Betreiber jederzeit frei, einen potenziellen Investor als Nutzer ohne Angabe weiterer Gründe abzulehnen.

Das Rechtsverhältnis zwischen Betreiber und der Blue Infrastructure GmbH (kurz: Emittent) ist nicht Gegenstand dieser AGB. Es bestimmt sich nach gesonderten Kooperationsverträgen.


## Registrierung
Um die Website vollumfänglich nutzen zu können, ist eine Registrierung erforderlich. Die Registrierung als Privatperson ist natürlichen Personen gestattet, die das 18. Lebensjahr vollendet haben und uneingeschränkt geschäftsfähig sind sowie juristischen Personen. Investoren müssen auf eigene Rechnung handeln. Die mehrfache Registrierung ein und derselben Person ist nicht gestattet.

Die Registrierung hat zwingend unter vollständiger wahrheitsgemäßer Angabe der abgefragten Daten zu erfolgen. Die Registrierung unter Angabe unrichtiger Daten oder die Angabe falscher investorenspezifischer Daten ist unzulässig und führt zum Ausschluss des Nutzers von der Website.

Der Nutzer verpflichtet sich während der Nutzung von www.blue.bi sämtliche Angaben zu seiner Person aktuell zu halten.

Der Nutzer verpflichtet sich, dafür Sorge zu tragen, dass seine Zugangsdaten, insbesondere sein Passwort, Dritten nicht zugänglich gemacht werden. Ausschließlich er ist verantwortlich für sämtliche über seinen Account ablaufenden Handlungen. Sofern Anhaltspunkte für den Missbrauch des Nutzeraccounts bestehen oder Dritte dennoch Kenntnis von den Zugangsdaten erlangt haben, ist der Nutzer verpflichtet, dies umgehend gegenüber dem www.blue.bi anzuzeigen.

Der Betreiber wird die Zugangsdaten des Nutzers nicht an Dritte weitergeben und diese nicht per E-Mail oder Telefon bei ihm abfragen.

## Leistungen des Betreibers
Der Betreiber bietet den Nutzern die Möglichkeit, über die Website Kontakt zum Emittenten mit Kapitalbedarf aufzunehmen.

Der Betreiber bietet dem Emittenten mit Kapitalbedarf die Möglichkeit, Informationen und Unterlagen auf der Website potenziellen Investoren zur Verfügung zu stellen. Außerdem bietet der Betreiber dem Emittenten die Möglichkeit, potenziellen Investoren über die Website ein rechtlich bindendes Angebot auf Abschluss eines Genussrechtsvertrages zu unterbreiten und entsprechende Verträge abzuschließen. Der Betreiber beschränkt sich in diesem Zusammenhang darauf, die technischen Rahmenbedingungen für den Abschluss der Verträge zur Verfügung zu stellen, den Versand von Unterlagen zu organisieren, Willenserklärungen als Bote zu übermitteln und dem Emittenten bestimmte weitere Dienstleistungen im Rahmen der Anbahnung und Abwicklung der Verträge zu erbringen. Insbesondere hat der Betreiber kein eigenes Handlungsermessen hinsichtlich des Abschlusses von Verträgen oder der Steuerung des Finanzierungsprozesses, wird nicht selbst Partei der Genussrechtsverträge, tritt beim Abschluss dieser Verträge nicht als Bevollmächtigter einer Partei auf und nimmt keine Zahlungen entgegen. Der Betreiber erbringt keine Dienstleistung, die eine Erlaubnis nach dem österreichischen Bankwesengesetz, dem österreichischen Wertpapieraufsichtsgesetz, dem österreichischen Kapitalmarktgesetz, dem österreichischen Alternativen Investmentfonds-Manager-Gesetz, dem österreichischen Zahlungsdienstegesetz, dem österreichischen Versicherungsaufsichtsgesetz oder dem österreichischen E-Geldgesetz oder dem deutschen Kreditwesengesetz, dem deutschen Kapitalanlagegesetzbuch oder dem deutschen Zahlungsdiensteaufsichtsgesetz erfordern. Zahlungen werden ausschließlich über einen Zahlungsdienstleister abgewickelt.

Der Zahlungsdienstleiter, MANGOPAY SA, Aktiengesellschaft luxemburgischen Rechts, mit Firmensitz in 2, Avenue Amélie, L-1125 Luxembourg und eingetragen im luxemburgischen Handels- und Firmenregister unter der Nummer B173459, berechtigt zur Ausübung ihrer Tätigkeit im Europäischer Wirtschaftsraum, als E-Geld-Institut zugelassen von der luxemburgischen Finanzmarktaufsichtsbehörde 283 route d’Arlon L-1150 Luxemburg, www.cssf.lu (kurz: Mangopay oder Zahlungsdienstleister), wird für den Genussrechtsberechtigten, nach dessen abgeschlossener und verifizierter Legitimierung über die Website, ein Zahlungskonto eröffnen, das durch Mangopay geführt wird (kurz: Guthabenkonto).

Mangopay stellt dem Genussrechtsberechtigten Zahlungsdienste, unter den, über die Website: https://blue.bi/documents/mangopay, veröffentlichten Bedingungen, bereit. Den Bedingungen, die zwischen dem Genussrechtsberechtigten und Mangopay geschlossen werden, kann über die Website zugestimmt werden.

Die Nutzung der Website ist für den Nutzer unentgeltlich. Es werden für Einzahlungen auf das Guthabenkonto Gebühren erhoben, die während des Einzahlungsprozesses auf der Website ersichtlich sind.

Der Betreiber übt keine Beratungstätigkeit aus und erbringt keine Beratungsleistungen. Insbesondere werden keine Finanzierungs- und/oder Anlageberatung sowie keine steuerliche und/oder rechtliche Beratung erbracht. Der Betreiber nimmt im Vorfeld des Einstellens eines Investments auf der Website lediglich eine Prüfung nach formalen Kriterien vor. Das Einstellen auf der Website stellt keine Investitionsempfehlung dar. Der Betreiber beurteilt nicht die Bonität des Emittenten und überprüft nicht die von diesem zur Verfügung gestellten Informationen auf ihren Wahrheitsgehalt, ihre Vollständigkeit oder ihre Aktualität. Der Betreiber gibt Investoren keine persönlichen Empfehlungen zum Erwerb von Finanzinstrumenten auf Grundlage einer Prüfung der persönlichen Umstände des jeweiligen Investors. Die persönlichen Umstände werden nur insoweit erfragt, wie dies im Rahmen der Anlagevermittlung vorgeschrieben ist, und lediglich mit dem Ziel, die gesetzlich vorgeschriebenen Hinweise zu erteilen, nicht aber mit dem Ziel, dem Investor eine persönliche Empfehlung zum Erwerb eines bestimmten Finanzinstruments auszusprechen.

Die auf der Website zur Verfügung gestellten Unterlagen erheben ausdrücklich nicht den Anspruch, alle Informationen zu enthalten, die für die Beurteilung der jeweils angebotenen Anlage erforderlich sind. Nutzer sollten die Möglichkeit wahrnehmen, dem Emittenten über die Website Fragen zu stellen, bevor sie eine Investitionsentscheidung treffen. Nutzer sollten sich aus unabhängigen Quellen informieren, wenn sie unsicher sind, ob sie einen Genussrechtsvertrag abschließen sollten. Eine fachkundige Beratung kann durch die auf der Website zur Verfügung gestellten Unterlagen nicht ersetzt werden. Nutzer sollten sich vor Abschluss eines qualifiziert nachrangigen Genussrechtsvertrags über die rechtlichen, wirtschaftlichen und steuerlichen Folgen eines solchen Investments informieren. Bei qualifiziert nachrangigen Genussrechten tragen Nutzer als Kapitalgeber ein 
(mit-) unternehmerisches Risiko, das höher ist als das Risiko eines regulären Fremdkapitalgebers. Das Genussrechtskapital einschließlich der Vergütungen kann aufgrund des qualifizierten Rangrücktritts nicht zurückgefordert werden, wenn dies für den Emittenten einen Insolvenzgrund herbeiführen würde. Dies kann zum Totalverlust des investierten Kapitals führen. Nutzer sollten die ausführlichen Risikohinweise beachten.

Das jeweilige Genussrechtskapital kann vom Nutzer im vorgegebenen Rahmen frei gewählt werden. Für ein Investment darf der Nutzer nur eigene liquide Mittel verwenden, die frei von Rechten Dritter sind.

Kommentare von Nutzern, die auf der Website bzw. den dazugehörigen Blogs etc. abgegeben werden und unangemessen sind oder gegen geltendes Recht verstoßen, sind nicht gestattet und werden umgehend gelöscht. Verstöße gegen diese Regelung können zu einer Schadensersatzverpflichtung des Nutzers sowie zum sofortigen Ausschluss des Nutzers von der Nutzung der Website führen.

Soweit der Nutzer dies dem Betreiber gestattet, stellt der Betreiber die vom Nutzer angegebenen Daten und/oder Informationen anderen Nutzern der Website zur Verfügung, sofern diese Daten und/oder Informationen nicht gegen gesetzliche Vorschriften oder diese AGB verstoßen. Der Betreiber behält sich vor, die entsprechenden Daten und/oder Informationen stichprobenartig zu prüfen

## Ablauf eines Investments
Die Genussrechte können durch natürliche oder juristische Personen erworben werden (kurz: Investor). 
Ein Angebot auf Erwerb von Genussrechten kann über die Website an den Emittenten gestellt werden. Eine Angebotsstellung über die Website erfolgt durch Auswahl eines Betrages in den jeweiligen Projektdetails, den der Interessent in Form von Genussrechten in das jeweilige Projekt investieren möchte und entsprechender Bestätigung auf der Website.

Die Annahme steht im freien Ermessen des Emittenten.

Die Annahme von Teilen des durch den Investor gewünschten Betrages möglich.

Ein Erwerb von Genussrechten führt zur Reduzierung des Guthabenkontos um den Wert für welchen Genussrechte erworben werden.

Die Genussrechte werden durch folgende Bedingungen erworben:
1.	Der Genussrechtsberechtigte bestimmt ein Projekt, für welches das Genussrechtskapital genutzt werden darf. Es darf nur so veranlagt werden, wie es der Investor/ Geldgeber jeweils konkret vorschreibt (Veranlagungsstrategie), dem Emittenten fehlt jeglicher Entscheidungsspielraum. 
2.	Der Genussrechtsberechtigte bestimmt die Höhe des Genussrechtskapitals, das von einem Projekt genutzt werden darf.
3.	Das gesamte Genussrechtskapital eines Projektes ist niedriger als der Projektkapitalbedarf desselben Projektes. Dieser Umstand tritt entweder ein:
a.	beim Start eines neuen Projektes (neuer Projektvorschlag), und dem Einsammeln von Kapital für dieses Projekt, ein oder
b.	wenn ein Investor Teile seines Genussrechtskapitals an einem Projekt kündigt und das Projekt dadurch wieder Genussrechtskapital benötigt, um den Projektkapitalbedarf des Projekts aufzufüllen.
c.	
Ist vom Emittenten ein Kapitalmarktprospekt zu veröffentlichen, können Interessenten während der Gültigkeit des Kapitalmarktprospektes ein Angebot zum Erwerb von Genussrechten stellen.

Der vom Genussrechtsinhaber gezeichnete Beteiligungsbetrag muss mindestens EUR 1,00 betragen.

Die Ausgabe eines Genussrechtes entspricht dem Betrag eines Euros. Auf die ausgegebenen Genussrechte wird vom Emittenten kein Agio erhoben.

Nähere Details zum Zeichnungsprozess sind in den Genussrechtsbedingungen veröffentlicht.

## Laufzeit und Kündigung
Der nach diesen AGB bestehende Nutzungsvertrag wird auf unbestimmte Zeit geschlossen. Er kann jeder-zeit durch den Nutzer oder den Betreiber mit einer Frist von einer Woche zum Monatsende gekündigt werden. Das Recht zur außerordentlichen Kündigung bleibt hiervon unberührt.

Kündigungen des Nutzers sind per E-Mail an rücktritt_agb@blue.bi zu richten. Der Nutzer erhält automatisiert eine Empfangsbestätigung bei Eingang der Nachricht vom Betreiber. Alternativ oder sollte der Nutzer beispielsweise aufgrund technischer Probleme keine Empfangsbestätigung erhalten, kann eine entsprechende Mitteilung auch an die auf der Website veröffentlichte Anschrift des Betreibers per Post gesendet werden.

Es wird klargestellt, dass eine Kündigung dieses Nutzungsvertrages bestehende Vertragsverhältnisse zwischen Nutzern und Emittent, insbesondere bestehende Genussrechtsverträge, nicht berührt.

## Verfügbarkeit
Der Betreiber ist bestrebt, im Rahmen des technisch Machbaren und wirtschaftlich Zumutbaren eine umfassende Verfügbarkeit der Website anzubieten. Der Betreiber übernimmt hierfür jedoch keine Gewährleistung. Insbesondere können Wartungsarbeiten, Sicherheits- und Kapazitätsgründe, technische Gegebenheiten sowie Ereignisse außerhalb des Herrschaftsbereichs des Betreibers zu einer vorübergehenden oder dauerhaften Nichterreichbarkeit der Website führen. Der Betreiber behält sich vor, den Zugang zur Website jederzeit und soweit jeweils erforderlich einzuschränken, dies z.B. zur Durchführung von Wartungsarbeiten.

## Vertraulichkeit
Der Nutzer ist verpflichtet, alle über die Website in passwortgeschützten Bereichen einsehbaren Informationen über den Emittenten, sowie jegliche weiteren Dokumente und insbesondere Verträge geheim zu halten und Dritten nicht zugänglich zu machen. Diese Informationen dürfen nur für die Entscheidung und Einschätzung des jeweiligen Investments genutzt werden. Das betrifft insbesondere die in den Unternehmensplänen und Unternehmensberichten bereitgestellten Informationen. Verstöße gegen diese Vorschrift können zu einer Schadensersatzverpflichtung des Nutzers und/oder zum sofortigen Ausschluss des Nutzers von der weiteren Nutzung der Website führen.

## Datenschutz
Die Erhebung und Verwendung von personenbezogenen Daten des Nutzers erfolgt ausschließlich im Rahmen der gesetzlichen Bestimmungen, insbesondere unter Berücksichtigung des geltenden Datenschutzrechts. Nähere Informationen hierzu gibt die gesonderte Datenschutzerklärung des Betreibers unter www.blue.bi/documents/privacy.

## Aufzeichnungen
Der Betreiber zeichnet bzw. bewahrt, aufgrund des sensiblen Geschäftsfelds der Finanzdienstleistungen Telefongespräche und die elektronische Kommunikation (z.B. E-Mail, Chat, VOIP, …) im Zusammenhang mit den Dienstleistungen des Betreibers oder Emittenten, auf. Die Aufzeichnungen werden über einen Zeitraum von fünf Jahren bzw. den gesetzlich vorgeschriebenen Zeitraum aufbewahrt. Auf Wunsch der Nutzer werden die Aufzeichnungen kostenfrei zur Verfügung gestellt. Vor Beginn der Aufzeichnung von Telefongesprächen wird der Nutzer darüber informiert, sollte der Nutzer nicht bereits seine Einwilligung erteilt haben. Die Aufzeichnungen können zu Beweiszwecken in etwaigen Rechtsstreitigkeiten verwendet werden.
Haftung
Die Haftung des Betreibers für vertragliche Pflichtverletzungen sowie aus deliktischer Haftung ist auf Vorsatz und grobe Fahrlässigkeit beschränkt.

Darüber hinaus haftet der Betreiber bei einfacher (leichter) Fahrlässigkeit nur bei Verletzung von solchen wesentlichen Vertragspflichten, deren Erfüllung die ordnungsgemäße Durchführung des Vertrages überhaupt erst ermöglicht und auf deren Einhaltung der Nutzer regelmäßig vertrauen darf ("Kardinalpflichten"). Die Haftung für Kardinalpflichten ist auf solche typischen Schäden und/oder einen solchen typischen Schadensumfang begrenzt, wie sie/er zum Zeitpunkt des Vertragsschlusses vorhersehbar war/en. 

Der Betreiber haftet weder für die Informationen, die auf der Website zur Verfügung gestellt werden, noch für die Wirksamkeit der zwischen dem Nutzer und dem Emittenten abgeschlossenen Genussrechtsverträge. Die auf der Website vom Emittenten zur Verfügung gestellten Informationen beruhen ausschließlich auf Aussagen und Unterlagen des Emittenten selbst. Die Verantwortung dafür, dass diese Informationen zutreffend, aktuell und vollständig sind, liegt allein beim Emittenten. Eine Prüfung der zur Verfügung gestellten Informationen durch den Betreiber erfolgt nicht.

Die Website enthält Links auf externe Webseiten. Auf die Inhalte dieser direkt oder indirekt verlinkten Webseiten hat der Betreiber keinen Einfluss. Für die Richtigkeit der Inhalte ist immer der jeweilige Anbieter oder Betreiber verantwortlich, weshalb der Betreiber diesbezüglich keinerlei Gewähr übernimmt. Eine ständige Überprüfung sämtlicher Inhalte der vom Betreiber verlinkten Seiten ohne tatsächliche Anhaltspunkte für einen Rechtsverstoß kann der Betreiber nicht leisten. Falls dem Betreiber Rechtsverletzungen bekannt werden, wird der Betreiber die entsprechenden Links sofort entfernen.

## Schlussbestimmungen
Der Betreiber behält sich vor, diese AGB jederzeit und ohne Angabe von Gründen für die Zukunft zu ändern. Registrierten Nutzern werden künftige Änderungen dieser AGB spätestens einen Monat vor Wirksamwerden der geänderten AGB per E-Mail bekannt gegeben. Widerspricht der Nutzer nicht innerhalb eines Monats nach Zugang der Bekanntgabe, so gelten die geänderten AGB als von ihm angenommen. Hierauf wird der Betreiber in der Bekanntmachung gesondert hinweisen.

Auf diese AGB findet ausschließlich österreichisches materielles Recht Anwendung. Vertragssprache und maßgebliche Sprache für die Kommunikation zwischen dem Betreiber und dem Nutzer ist Deutsch.

Diese AGB und das Rechtsverhältnis zwischen dem Betreiber und dem Nutzer unterliegen ausschließlich österreichischem materiellem Recht. Es gilt der Gerichtsstand des Betreibers. 

Sollten eine oder mehrere Regelungen dieser AGB ganz oder teilweise unwirksam oder undurchsetzbar sein, werden die Wirksamkeit und Durchsetzbarkeit aller übrigen Bestimmungen dieses Vertrages davon nicht berührt. Die unwirksame oder undurchsetzbare Bestimmung ist als durch diejenige wirksame und durchsetzbare Bestimmung als ersetzt anzusehen, die dem von den Parteien mit der unwirksamen oder undurchsetzbare Bestimmung verfolgten wirtschaftlichen Zweck am nächsten kommt.

