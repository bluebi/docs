# Risikohinweise
## Allgemeine Risikohinweise
Bei der gegenständlichen Investition, mittels nachrangiger Genussrechte, handelt es sich um eine langfristige Investition. Diese Investitionsform ist mit Chancen und Risiken verbunden. Es kann zum vollständigen Verlust des eingesetzten Betrags führen. Das Risiko beschränkt sich jedoch auf die getätigte Investitionssumme und es besteht somit keine Nachschusspflicht. Es können keine Zusagen oder verlässliche Prognosen über künftige Erträge gemacht werden. Insbesondere stellen etwaige erwirtschaftete Erträge in der Vergangenheit keinen Indikator für künftige Erträge dar. Blue Investments GmbH als Betreiber der Crowdinvesting-Plattform übernimmt keine Haftung für die Richtigkeit und Aktualität der Inhalte. Um das Risiko zu reduzieren, sollte in unterschiedliche Investitionsobjekte investiert werden, um das Risiko zu streuen. Es sollten nur Geldbeträge investiert werden, deren Verlust finanziell leistbar ist und die während der Investition nicht liquide benötigt oder zurückerwartet werden.

## Es kommen insbesondere folgende Risiken zum Tragen:

### Risiko des Totalverlustes
Unter dem Risiko des Totalverlustes versteht man das Risiko, dass ein Investment wertlos werden kann. Ein Totalverlust kann insbesondere dann eintreten, wenn der Emittent aus wirtschaftlichen oder rechtlichen Gründen nicht mehr in der Lage ist, seinen Zahlungsverpflichtungen nachzukommen (Insolvenz). Eine Zahlungsunfähigkeit des Emittenten führt regelmäßig zu einem Totalverlust. 

### Bonitäts- oder Emittentenrisiko
Unter Bonitätsrisiko versteht man die Gefahr der Zahlungsunfähigkeit (Insolvenz) des Emittenten, d.h. eine mögliche Unfähigkeit zur termingerechten oder endgültigen Erfüllung seiner Verpflichtungen wie Vergütungszahlungen. 

### Malversationsrisiko
Unter Malversationsrisiko versteht man das Risiko, dass es beim Emittenten oder Partnern zu strafbaren Handlungen von Mitarbeitern/Organen kommt. Diese können nie zur Gänze ausgeschlossen werden. Malversationen können mittelbar oder unmittelbar schädigen und auch zur Insolvenz führen.

### Klumpenrisiko
Darunter versteht man jenes Risiko, das entsteht, wenn man keine oder nur eine geringe Streuung des Portfolios vornimmt. Von einem Investment in nur wenige Titel ist daher abzuraten.

### Erschwerte Übertragbarkeit
Darunter ist zu verstehen, dass die Investition nur unter besonderen Bedingungen übertragbar ist.

### Wichtige Hinweise
* Dieses öffentliche Angebot von Veranlagungen wurde weder von der Finanzmarktaufsicht (FMA) noch einer anderen österreichischen Behörde geprüft oder genehmigt. 
*Die Investition fällt nicht unter die gesetzlichen Einlagensicherungs- und Anlegerentschädigungssysteme. 
* Es handelt sich nicht um ein Sparprodukt. Es sollten nicht mehr als 10% des Nettovermögens in solche Wertpapiere oder Veranlagungen investiert werden.
* Blue Investments GmbH führt keine Anlagenberatung durch. Sofern dennoch ein Auftrag an Blue Investments GmbH erteilt wird, geschieht dies im Rahmen des so genannten beratungsfreien Geschäfts.